"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = void 0;
const uuid_1 = require("uuid");
const role_schema_1 = require("../role/role.schema");
const express_1 = __importDefault(require("express"));
class UserSchema {
    static create(user, isAdmin = false) {
        try {
            const { roleId } = user;
            const role = role_schema_1.RoleSchema.roles.find(r => r.id === roleId);
            if (!role) {
                throw { message: "Invalid role id", stack: express_1.default };
            }
            const isAdminrole = role.name === "admin";
            if (isAdmin) {
                user.roleId = 1;
            }
            if (isAdminrole && !isAdmin) {
                const existingAdmin = UserSchema.users.find(u => u.roleId === roleId && role_schema_1.RoleSchema.roles[u.roleId - 1].name === "admin");
                if (existingAdmin)
                    throw { message: "There can only be one admin", stack: express_1.default };
            }
            const id = (0, uuid_1.v4)();
            const userRecord = Object.assign(Object.assign({}, user), { id });
            UserSchema.users.push(userRecord);
            return userRecord;
        }
        catch (_a) {
            throw { message: "record not created", stack: express_1.default };
        }
    }
    static findOne(cb) {
        return UserSchema.users.find(cb);
    }
    static findAll() {
        return UserSchema.users;
    }
}
exports.UserSchema = UserSchema;
UserSchema.users = [];
