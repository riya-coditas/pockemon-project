"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("./user.schema");
const create = (user, isAdminCreatingUser) => user_schema_1.UserSchema.create(user, isAdminCreatingUser);
const findOne = (cb) => user_schema_1.UserSchema.findOne(cb);
const findAll = () => user_schema_1.UserSchema.findAll();
exports.default = {
    create,
    findOne,
    findAll
};
