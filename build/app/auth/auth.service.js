"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_service_1 = __importDefault(require("../user/user.service"));
const role_service_1 = __importDefault(require("../role/role.service"));
const auth_responses_1 = require("./auth.responses");
const bcryptjs_1 = require("bcryptjs");
const pockemon_schema_1 = require("../pockemon/pockemon.schema");
const pockemon_service_1 = __importDefault(require("../pockemon/pockemon.service"));
const user_schema_1 = require("../user/user.schema");
const encryptUserPassword = (user) => __awaiter(void 0, void 0, void 0, function* () {
    const salt = yield (0, bcryptjs_1.genSalt)(10);
    const hashedPassword = yield (0, bcryptjs_1.hash)(user.password, salt);
    user.password = hashedPassword;
    return user;
});
const register = (user, isAdmin) => __awaiter(void 0, void 0, void 0, function* () {
    user = yield encryptUserPassword(user);
    user.roleId = 2;
    user.pockemon = [];
    return user_service_1.default.create(user, isAdmin);
});
const findall = (credential) => __awaiter(void 0, void 0, void 0, function* () {
    const user = user_service_1.default.findOne(u => u.email === credential.email);
    if ((user === null || user === void 0 ? void 0 : user.roleId) == 1) {
        const users = user_service_1.default.findAll();
        return Object.assign({}, users);
    }
    else {
        throw auth_responses_1.AUTH_RESPONSES.NOT_ADMIN;
    }
});
const createRole = (role) => __awaiter(void 0, void 0, void 0, function* () {
    return role_service_1.default.create(role);
});
const createPockemon = (pockemon) => __awaiter(void 0, void 0, void 0, function* () {
    return pockemon_service_1.default.create(pockemon);
});
const login = (credential) => __awaiter(void 0, void 0, void 0, function* () {
    const user = user_service_1.default.findOne(u => u.email === credential.email);
    if (!user)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const isPasswordValid = yield (0, bcryptjs_1.compare)(credential.password, user.password);
    if (!isPasswordValid)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const { password } = user, userClone = __rest(user, ["password"]);
    return userClone;
});
const findTrainer = (credential) => __awaiter(void 0, void 0, void 0, function* () {
    const trainer = user_service_1.default.findOne(u => u.email === credential.email);
    if (!trainer)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const pokemonclone = __rest(trainer, []);
    const pockemon = pokemonclone.pockemon;
    return pockemon;
});
const assignTrainer = (id, name) => __awaiter(void 0, void 0, void 0, function* () {
    const user = user_schema_1.UserSchema.users.find(u => u.id === id);
    console.log(user);
    if (!user) {
        console.log("Trainer not found");
        return;
    }
    const pockemon = pockemon_schema_1.PockemonSchema.pockemon.find(p => p.name === name);
    if (!pockemon) {
        console.log("Pockemon not found");
        return;
    }
    user.pockemon.push(pockemon);
    return user;
});
const findRole = (credential) => __awaiter(void 0, void 0, void 0, function* () {
    const role = role_service_1.default.findOne(r => r.name === credential.name);
    if (!role)
        throw auth_responses_1.AUTH_RESPONSES.INVALID_CREDENTIALS;
    const roleClone = __rest(role, []);
    return roleClone;
});
const countPockemon = (credential) => {
    const user = user_service_1.default.findOne(u => u.email === credential.email);
    if ((user === null || user === void 0 ? void 0 : user.roleId) == 1) {
        const count = pockemon_schema_1.PockemonSchema.countPokemon();
        return count;
    }
    else {
        throw auth_responses_1.AUTH_RESPONSES.NOT_ADMIN;
    }
};
exports.default = {
    register,
    createRole,
    login,
    findRole,
    createPockemon,
    findall,
    findTrainer,
    assignTrainer,
    countPockemon
};
