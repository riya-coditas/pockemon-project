"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginValidator = void 0;
//import { body } from "../utility/validate";
const express_validator_1 = require("express-validator");
exports.LoginValidator = [
    (0, express_validator_1.body)("email").isEmail().withMessage("email is required"),
    (0, express_validator_1.body)("password").isString().isLength({ min: 6 }).withMessage("password is required")
];
