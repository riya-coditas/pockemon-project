"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const pockemon_service_1 = __importDefault(require("../pockemon/pockemon.service"));
const response_handler_1 = require("../utility/response-handler");
const auth_service_1 = __importDefault(require("./auth.service"));
const auth_validations_1 = require("./auth.validations");
const router = (0, express_1.Router)();
router.post("/register", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = req.body;
        const isAdmin = req.query.isAdmin === 'true';
        const result = yield auth_service_1.default.register(user, isAdmin);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.post("/registerRole", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const role = req.body;
        const result = yield auth_service_1.default.createRole(role);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.get("/findrole", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const name = req.body;
        const result = yield auth_service_1.default.findRole(name);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.get("/findAll", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const email = req.body;
        const result = yield auth_service_1.default.findall(email);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.get("/count", (req, res, next) => {
    try {
        const email = req.body;
        const result = auth_service_1.default.countPockemon(email);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
router.post("/createPockemon", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const pockemon = req.body;
        const result = yield auth_service_1.default.createPockemon(pockemon);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.get("/viewPockemon", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield pockemon_service_1.default.viewAllPokemons();
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.put('/:id/pockemon/:name', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id, name } = req.params;
    const result = yield auth_service_1.default.assignTrainer(id, name);
    res.send(new response_handler_1.ResponseHandler(result));
}));
router.get("/findTrainer", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const email = req.body;
        const result = yield auth_service_1.default.findTrainer(email);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
router.post("/login", auth_validations_1.LoginValidator, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const credentials = req.body;
        const result = yield auth_service_1.default.login(credentials);
        res.send(new response_handler_1.ResponseHandler(result));
    }
    catch (e) {
        next(e);
    }
}));
exports.default = router;
