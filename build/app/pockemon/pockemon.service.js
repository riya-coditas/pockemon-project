"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pockemon_repo_1 = __importDefault(require("./pockemon.repo"));
const create = (pockemon) => pockemon_repo_1.default.create(pockemon);
const viewAllPokemons = () => pockemon_repo_1.default.viewAllPokemons();
const countPockemon = () => pockemon_repo_1.default.countPockemon();
exports.default = {
    create,
    viewAllPokemons,
    countPockemon
};
