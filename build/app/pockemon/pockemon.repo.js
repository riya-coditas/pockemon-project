"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pockemon_schema_1 = require("./pockemon.schema");
const create = (pockemon) => pockemon_schema_1.PockemonSchema.create(pockemon);
const viewAllPokemons = () => pockemon_schema_1.PockemonSchema.viewAllPokemons();
const countPockemon = () => pockemon_schema_1.PockemonSchema.countPokemon();
exports.default = {
    create,
    viewAllPokemons,
    countPockemon
};
