"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PockemonSchema = void 0;
const express_1 = __importDefault(require("express"));
const uuid_1 = require("uuid");
class PockemonSchema {
    static create(pockemon) {
        try {
            const id = (0, uuid_1.v4)();
            const pockemonRecord = Object.assign(Object.assign({}, pockemon), { id });
            PockemonSchema.pockemon.push(pockemonRecord);
            return pockemonRecord;
        }
        catch (_a) {
            throw { message: "record not created", stack: express_1.default };
        }
    }
    static viewAllPokemons() {
        return PockemonSchema.pockemon;
    }
    static countPokemon() {
        let count = PockemonSchema.pockemon.length;
        // console.log(count)
        return count;
    }
}
exports.PockemonSchema = PockemonSchema;
PockemonSchema.pockemon = [];
