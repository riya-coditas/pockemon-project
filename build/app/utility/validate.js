"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validate = exports.query = exports.params = exports.body = void 0;
const express_validator_1 = require("express-validator");
// factory function
// part = "body", field = "email"
const check = (part, field) => (req, res, next) => {
    if (req[part].hasOwnProperty(field)) {
        return next();
    }
    next({ message: `${field} is required` });
};
const body = (field) => check("body", field);
exports.body = body;
const params = (field) => check("params", field);
exports.params = params;
const query = (field) => check("query", field);
exports.query = query;
class ValidationMethods {
    constructor(value) {
        this.value = value;
        this.errors = [];
    }
    ;
    isString() {
        if (typeof this.value !== 'string') {
            this.errors.push({ message: 'type needs to be' });
        }
        return this;
    }
}
const validate = (req, res, next) => {
    const validateResult = express_validator_1.validationResult;
};
exports.validate = validate;
