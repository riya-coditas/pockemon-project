"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleSchema = void 0;
class RoleSchema {
    static create(role) {
        try {
            const id = RoleSchema.roles.length + 1;
            const roleRecord = Object.assign(Object.assign({}, role), { id });
            RoleSchema.roles.push(roleRecord);
            return roleRecord;
        }
        catch (e) {
            throw { message: "record not created", stack: e };
        }
    }
    static findOne(cb) {
        return RoleSchema.roles.find(cb);
    }
}
exports.RoleSchema = RoleSchema;
RoleSchema.roles = [];
