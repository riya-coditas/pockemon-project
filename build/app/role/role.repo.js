"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const role_schema_1 = require("./role.schema");
const create = (role) => role_schema_1.RoleSchema.create(role);
const findOne = (cb) => role_schema_1.RoleSchema.findOne(cb);
exports.default = {
    create,
    findOne
};
