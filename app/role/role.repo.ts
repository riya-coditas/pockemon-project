import { RoleSchema } from "./role.schema";
import { IRole, RolePredicate } from "./role.type";

const create = (role: IRole) => RoleSchema.create(role);

const findOne = (cb: RolePredicate) => RoleSchema.findOne(cb); 

export default {
    create,
    findOne
}