import roleRepo from "./role.repo";
import { IRole, RolePredicate } from "./role.type";

const create = (role: IRole) => roleRepo.create(role);

const findOne = (cb: RolePredicate) => roleRepo.findOne(cb);

export default {
    create,
    findOne
}