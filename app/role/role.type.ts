import { RoleSchema } from "./role.schema";

export interface IRole {
    id: number;
    name: string;
}

export type Roles = IRole[];

export type RolePredicate = (r: IRole) => boolean




