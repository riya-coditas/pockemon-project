import { IRole, RolePredicate, Roles } from "./role.type";

export class RoleSchema {
    static roles: Roles = []

    static create(role: IRole) {
        try {
            const id = RoleSchema.roles.length + 1;
            const roleRecord = { ...role, id }
            RoleSchema.roles.push(roleRecord);
            return roleRecord;
            
        } catch (e) {
            throw { message: "record not created", stack: e }
        }
    }

    static findOne(cb: RolePredicate) {
        return RoleSchema.roles.find(cb);
    }
}
 