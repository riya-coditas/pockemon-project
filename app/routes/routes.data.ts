import {Route , Routes} from './routes.types'
import AuthRoutes from '../auth/auth.routes'


export const routes : Routes = [
    new Route("/auth", AuthRoutes)
]