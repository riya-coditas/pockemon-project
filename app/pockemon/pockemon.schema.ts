import e from "express";
import { v4 } from "uuid";
import { IPockemon, Pockemon } from "./pockemon.types";

export class PockemonSchema{
    static pockemon: Pockemon = []; 

    static create(pockemon: IPockemon) 
    {
        try{
           const id = v4();
           const pockemonRecord = { ...pockemon, id };
           PockemonSchema.pockemon.push(pockemonRecord);
           return pockemonRecord;
        }catch{
            throw { message: "record not created", stack:  e}
      }

    }

    static viewAllPokemons(): IPockemon[] {
        return PockemonSchema.pockemon;
  }

  static countPokemon(){
    let count = PockemonSchema.pockemon.length
   // console.log(count)
    return count
  }
}

