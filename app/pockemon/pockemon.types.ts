export interface IPockemon {
    id: string;
    name: string;
    imgURL: string;
}

export type Pockemon = IPockemon [];

export type PockemonPredicate = (p: IPockemon) => boolean