import { PockemonSchema } from "./pockemon.schema";
import { IPockemon } from "./pockemon.types";


const create = (pockemon: IPockemon) => PockemonSchema.create(pockemon);

const viewAllPokemons = () => PockemonSchema.viewAllPokemons()

const countPockemon = () => PockemonSchema.countPokemon()

export default {
    create,
    viewAllPokemons,
    countPockemon
}