
import pockemonRepo from "./pockemon.repo";
import { IPockemon } from "./pockemon.types";


const create = (pockemon: IPockemon) => pockemonRepo.create(pockemon);

const viewAllPokemons = () => pockemonRepo.viewAllPokemons()

const countPockemon = () => pockemonRepo.countPockemon()

export default {
    create,
    viewAllPokemons,
    countPockemon
}
