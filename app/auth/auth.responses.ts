export const AUTH_RESPONSES = {
    INVALID_CREDENTIALS: {
        statusCode: 400,
        message: "Invalid Credentials" 
    },

    NOT_ADMIN:{
        statusCode: 400,
        message:"You are not an admin!"
    }

     
}