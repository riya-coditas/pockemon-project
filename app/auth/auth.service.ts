import userService from "../user/user.service";
import roleService from "../role/role.service"
import { IUser } from "../user/user.types";
import { AUTH_RESPONSES } from "./auth.responses";
import { ICredentials, IRoleCredentials } from "./auth.types";
import { genSalt, hash, compare } from 'bcryptjs';
import { IRole } from "../role/role.type";
import { IPockemon } from "../pockemon/pockemon.types";
import { PockemonSchema } from "../pockemon/pockemon.schema";
import pockemonService from "../pockemon/pockemon.service";
import { UserSchema } from "../user/user.schema";

const encryptUserPassword = async (user: IUser) => {
    const salt = await genSalt(10);
    const hashedPassword = await hash(user.password, salt);
    user.password = hashedPassword;

    return user;
}

const register = async (user: IUser,isAdmin:boolean) => {
    user = await encryptUserPassword(user);
    user.roleId = 2
    user.pockemon=[]
    return userService.create(user,isAdmin);
}


const findall = async (credential: ICredentials) => {
    const user = userService.findOne(u => u.email === credential.email);
    if(user?.roleId == 1){
        const users = userService.findAll()
        return {...users}
    }
    else{
        throw AUTH_RESPONSES.NOT_ADMIN;
    }
}


const createRole = async (role: IRole) => {
    return roleService.create(role);
}

const createPockemon = async (pockemon: IPockemon) => {
    return pockemonService.create(pockemon);
}

const login = async (
    credential: ICredentials
) => {
    const user = userService.findOne(u => u.email === credential.email);

    if(!user) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

    const isPasswordValid = await compare(credential.password, user.password);
    if(!isPasswordValid) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

    const { password, ...userClone } = user;

    return userClone;
};


const findTrainer = async(
    credential: ICredentials
)=>{
    const trainer = userService.findOne(u =>u.email===credential.email)
    if(!trainer) throw AUTH_RESPONSES.INVALID_CREDENTIALS;
    const {...pokemonclone} = trainer
    const pockemon = pokemonclone.pockemon
    return pockemon
}

const assignTrainer = async(id:string,name:string)=>{
    const user = UserSchema.users.find(u => u.id === id);
    console.log(user)
    if (!user) {
        console.log("Trainer not found");
        return;
    }

    const pockemon = PockemonSchema.pockemon.find(p => p.name === name);
    if (!pockemon) {
        console.log("Pockemon not found");
        return;
    }
    
    user.pockemon.push(pockemon);
    return user;
}


const findRole = async (
    credential: IRoleCredentials
) => {
    const role = roleService.findOne(r => r.name === credential.name);

    if(!role) throw AUTH_RESPONSES.INVALID_CREDENTIALS;

    const {...roleClone } = role;

    return roleClone;
};


const countPockemon = (credential: ICredentials) => {
    const user = userService.findOne(u => u.email === credential.email);
    if(user?.roleId == 1){
        const count = PockemonSchema.countPokemon()
        return count
    }
    else{
        throw AUTH_RESPONSES.NOT_ADMIN;
    }
}

export default {
    register,
    createRole,
    login,
    findRole,
    createPockemon,
    findall,
    findTrainer,
    assignTrainer,
    countPockemon
}