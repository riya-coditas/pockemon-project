//import { body } from "../utility/validate";
import { body } from 'express-validator';

export const LoginValidator = [
    body("email").isEmail().withMessage("email is required"),

    body("password").isString().isLength({min:6}).withMessage("password is required")
]

