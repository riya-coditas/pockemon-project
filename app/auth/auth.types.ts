export interface ICredentials {
    email: string;
    password: string;
}

export interface IRoleCredentials{
    id:string;
    name:string;
}