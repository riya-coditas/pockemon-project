import { NextFunction, Request, Response, Router } from 'express';
import pockemonService from '../pockemon/pockemon.service';
import { IUser } from '../user/user.types';
import { ResponseHandler } from '../utility/response-handler';
import authService from './auth.service';
import { LoginValidator } from './auth.validations';

const router = Router();

router.post("/register", async (req, res, next) => {
    try {
        const user = req.body;
        const isAdmin = req.query.isAdmin === 'true';

        const result = await authService.register(user, isAdmin);
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

router.post("/registerRole", async (req, res, next) => {
    try {
        const role = req.body;
        const result = await authService.createRole(role);
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

router.get("/findrole", async (req, res, next) => {
    try {
        const name = req.body;
        const result = await authService.findRole(name)
        res.send(new ResponseHandler(result))
    } catch (e) {
        next(e)
    }
})

router.get("/findAll", async (req, res, next) => {
    try {
        const email = req.body 
        const result = await authService.findall(email)
        res.send(new ResponseHandler(result))
    } catch (e) {
        next(e)
    }
})

router.get("/count", (req, res, next) => {
    try {
        const email = req.body
        const result = authService.countPockemon(email)
        res.send(new ResponseHandler(result))
    } catch (e) {
        next(e)
    }
})


router.post("/createPockemon", async (req, res, next) => {
    try {
        const pockemon = req.body;
        const result = await authService.createPockemon(pockemon);
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

router.get("/viewPockemon", async (req, res, next) => {
    try {
        const result = await pockemonService.viewAllPokemons()
        res.send(new ResponseHandler(result))
    } catch (e) {
        next(e)
    }
})

router.put('/:id/pockemon/:name', async (req, res) => {
    const { id, name } = req.params;
    const result = await authService.assignTrainer(id,name)
    res.send(new ResponseHandler(result));
});

router.get("/findTrainer", async (req, res, next) => {
    try {
        const email = req.body;
        const result = await authService.findTrainer(email)
        res.send(new ResponseHandler(result))
    } catch (e) {
        next(e)
    }
})

router.post("/login", LoginValidator, async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const credentials = req.body;
        const result: Omit<IUser, "password"> = await authService.login(credentials);
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

export default router;