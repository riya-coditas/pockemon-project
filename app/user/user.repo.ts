import { IPockemon } from "../pockemon/pockemon.types";
import { UserSchema } from "./user.schema";
import { IUser, UserPredicate } from "./user.types";


const create = (user: IUser,isAdminCreatingUser:boolean) => UserSchema.create(user,isAdminCreatingUser);

const findOne = (cb: UserPredicate) => UserSchema.findOne(cb); 

const findAll = () => UserSchema.findAll()

export default {
    create,
    findOne,
    findAll
 
}