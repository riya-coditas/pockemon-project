import { IPockemon } from "../pockemon/pockemon.types";
import userRepo from "./user.repo";
import { IUser, UserPredicate } from "./user.types";

const create = (user: IUser,isAdminCreatingUser:boolean) => userRepo.create(user,isAdminCreatingUser);

const findOne = (cb: UserPredicate) => userRepo.findOne(cb);

const findAll = () => userRepo.findAll()

export default {
    create,
    findOne,
    findAll
}