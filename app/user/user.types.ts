export interface IUser {
    id: string;
    email: string;
    password: string;
    roleId:number;
    pockemon:any[];
}

export type Users = IUser[];

export type UserPredicate = (u: IUser) => boolean

