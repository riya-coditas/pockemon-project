import { IUser, UserPredicate, Users } from "./user.types";
import { v4 } from "uuid";
import { RoleSchema } from "../role/role.schema";
import e from "express";


export class UserSchema {
    static users: Users = []; 

    static create(user: IUser, isAdmin: boolean = false) 
    {
        try{
        const { roleId } = user;
         const role = RoleSchema.roles.find(r => r.id === roleId);
       
        if (!role) {
            throw { message: "Invalid role id", stack:  e}
        }
      
        const isAdminrole = role.name === "admin";
        if(isAdmin){
          user.roleId = 1
        }
    
        if (isAdminrole && !isAdmin) {

          const existingAdmin = UserSchema.users.find(u => u.roleId === roleId  && RoleSchema.roles[u.roleId - 1].name === "admin");
        
          if (existingAdmin) 
          throw { message: "There can only be one admin", stack:  e} 
        }
      
        const id = v4();
        const userRecord = { ...user, id };
        UserSchema.users.push(userRecord);
        return userRecord;
      }catch{
            throw { message: "record not created", stack:  e}
      }
 }


    static findOne(cb: UserPredicate) {
        return UserSchema.users.find(cb);
    }

      static findAll(): IUser[] {
        return UserSchema.users;
  }
}
        