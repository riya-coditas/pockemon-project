import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";

type Part = "body" | "query" | "params";

// factory function
// part = "body", field = "email"
const check = (part: Part, field: string) => (req: Request, res: Response, next: NextFunction) => {
    if (req[part].hasOwnProperty(field)) {
        return next();
    }

    next({ message: `${field} is required` });
}

export const body = (field: string) => check("body", field);
export const params = (field: string) => check("params", field);
export const query = (field: string) => check("query", field);

class ValidationMethods {
    private errors: any[] = [];
    constructor(private value: any) {};

    isString() {
        if(typeof this.value !== 'string') {
            this.errors.push({ message: 'type needs to be' });
        } 

        return this;
    }
}

export const validate = (req:Request,res:Response,next:NextFunction)=>{
    const validateResult = validationResult
}